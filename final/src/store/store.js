import { createStore, applyMiddleware } from 'redux';
import { middlewares } from './middlewares';
import { rootReducer } from '../reducer/rootReducer';

const store = createStore(rootReducer, applyMiddleware(...middlewares));

export default store;