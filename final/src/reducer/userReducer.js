import { updateUser } from "../utils/updateUser";
import { users } from "../data/users";

const initialState = {
  user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null,
  users:  localStorage.getItem('users') ? JSON.parse(localStorage.getItem('users')) : users,
  err: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOG_OUT':{ 
      const updatedUsers = state.users.map(user =>
        user.userName === state.user.userName ? state.user : user
      );
      updateUser(null, updatedUsers)
      return { ...state, user: null, users: updatedUsers};}
     

    case 'SING_IN': {
      const { userName, password } = action.payload;
      const user = state.users.find(
        (user) => user.userName === userName && user.password === password
      );
      if(user){
      updateUser(user)
      return { ...state, user: user, err: null };
      } else{
        return {...state, err: "User not found / password is wrong"}
      }
    
    };
    
    case 'SING_UP':{
      let checkFreeUserName = state.users.find(user => user.userName === action.payload.newUser.userName)
      if (checkFreeUserName){
        return {...state, err: "username is taken"}
      } else {
          updateUser( action.payload.newUser)
        return { ...state, user: action.payload.newUser, users: [...state.users, action.payload.newUser], err: null };
      }
    }
    
    case 'ADD_FAVORIT': {
     let updatedUser = { ...state.user, favoritesMovies: [...state.user.favoritesMovies, action.payload.movieId] };
      updateUser(updatedUser);
      return { ...state, user: updatedUser };
    }

    case 'REMOVE_FAVORIT': {
     let updatedUser = {
        ...state.user,
        favoritesMovies: state.user.favoritesMovies.filter(id => id !== action.payload.movieId)
      };
      updateUser(updatedUser);
      return { ...state, user: updatedUser };
    }
      
    default:
      return state;
  }
};

export default userReducer;
