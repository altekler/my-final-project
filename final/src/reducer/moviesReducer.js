const initialState = {
  movies: null,
  totalPages: null,
  currentPage: 1,
  movie: null,
  video: null,
  genres: null,
  languages: null,
  params: null,
};

const moviesReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_MOVIES':
      return { ...state, movies: action.payload.movies, totalPages: action.payload.totalPages };
    case 'LOAD_FAVORITE_MOVIE':
      return { ...state, movies: action.payload.movies };
    case 'LOAD_MOVIE':
      return { ...state, movie: action.payload.movie };
    case 'LOAD_VIDEO':
      return { ...state, video: action.payload.video };
    case 'LOAD_GENRES':
      return { ...state, genres: action.payload.genres };
      case 'LOAD_LANGUAGES':
        return { ...state, languages: action.payload.languages };
    case 'LOAD_PARAMS':
      return { ...state, params: action.payload.params, currentPage: 1 };
    case 'REMOVE_PARAMS':
      return { ...state, params: null, currentPage: 1 };
    case 'SET_CURRENT_PAGE':
      return { ...state, currentPage: action.payload.page };
    default:
      return state;
  }
};

export default moviesReducer;
