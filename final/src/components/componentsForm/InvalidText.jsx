import React from "react";
import Typography from '@mui/material/Typography';


const InvalidText = ({ children }) => {
    return <Typography variant="caption" display="block" gutterBottom sx={{
        color: 'red',
        mt: '-20px'
    }}>{children}</Typography>
}
export default InvalidText;
