import React, { forwardRef } from 'react';
import FormControlLabel from '@mui/material/FormControlLabel';
import Radio from '@mui/material/Radio';

const RadioButton = forwardRef((props, ref) => (
  <FormControlLabel
    {...props}
    control={
      <Radio
        sx={{
          '&.MuiRadio-root': {
            color: '#1976d2'
          },
        }}
      />
    }
    inputRef={ref}
  />
));

export default RadioButton;
