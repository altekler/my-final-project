import React, { forwardRef } from 'react';
import TextField from '@mui/material/TextField';

const FormInput = forwardRef((props, ref) => (
  <TextField
    {...props}
    variant="filled"
    fullWidth
    inputRef={ref}
    sx={{
      mb: 3,
      '& .MuiInputBase-input': {
        color: 'white',
      },
      '& .MuiInputLabel-root': {
        color: '#1976d2',
      }
    }}
  />
));

export default FormInput;