import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import FormInput from "../componentsForm/FormInput";
import './Search.scss'
import { useForm } from "react-hook-form";
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import IconButton from '@mui/material/IconButton';
import SearchIcon from '@mui/icons-material/Search';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { fetchGenres, fetchLanguage } from '../../thunk/movies';
import Checkbox from '@mui/material/Checkbox';
import FormControlLabel from '@mui/material/FormControlLabel';
import { loadParams, removeParams } from '../../actions/moviesActions';
function Search() {
    const dispatch = useDispatch();
    const params = useSelector((state) => state.moviesReducer.params);
    const { register, handleSubmit, watch, setValue, reset } = useForm(
        {
            defaultValues: {
                language: ""
            },
        }
    );
    const onSubmit = data => {
        let chackedGenres = data.genres.filter(genre => genre).map(genre => parseInt(genre));
        let paramsNew = { genres: chackedGenres, language: data.language, query: data.query }
        dispatch(loadParams(paramsNew))
        reset()
    }
    const genresData = useSelector(state => state.moviesReducer.genres);
    const genresArr = useMemo(() => genresData || [], [genresData]);

    const languagesData = useSelector(state => state.moviesReducer.languages);
    const languagesArr = useMemo(() => languagesData || [], [languagesData]);


    const handleChange = (event) => {
        setValue("language", event.target.value);
    };
    const watchedLanguage = watch("language");

    useEffect(() => {
        dispatch(fetchGenres());
        dispatch(fetchLanguage())
    }, [dispatch]);

    const handleRemoveParams = () => {
        dispatch(removeParams())
    }
    return (
        <>
            <div className='section-search'>
                <form onSubmit={handleSubmit(onSubmit)} className='movies-filter'>
                    <FormInput
                        label="Search"
                        type="text"
                        style={{ width: "300px", marginLeft: "24px" }}
                        {...register("query")}
                    />
                    <PopupState variant="popover" popupId="demo-popup-menu">
                        {(popupState) => (
                            <React.Fragment>
                                <Button variant="outlined" sx={{ height: '55px' }}{...bindTrigger(popupState)}>
                                    Gengre
                                </Button>
                                <Menu {...bindMenu(popupState)}>
                                    {genresArr.map((genre) => (
                                        <MenuItem key={genre.id}> <FormControlLabel
                                            control={<Checkbox  {...register(`genres.${genre.id}`)} />}
                                            value={genre.id}
                                            label={genre.name}
                                        /></MenuItem>
                                    )
                                    )}
                                </Menu>
                            </React.Fragment>
                        )}
                    </PopupState>
                    <FormControl sx={{ width: "300px" }}>
                        <InputLabel id="demo-simple-select-label">Language</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={watchedLanguage}
                            label="Language"
                            onChange={handleChange}
                            {...register("language")}
                            sx={{
                                mb: 3,
                                '& .MuiInputBase-input': {
                                    color: 'white',
                                },
                                '& .MuiInputLabel-root': {
                                    color: '#1976d2',
                                }
                            }}
                        >
                            {languagesArr.map((language) => (
                                <MenuItem key={language.iso_639_1} value={language.iso_639_1}>
                                    {language.english_name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <IconButton color="primary" component="label" size='large' sx={{ height: '55px' }} onClick={handleSubmit(onSubmit)}>
                        <SearchIcon />
                    </IconButton>

                </form>
                {params ? <Button color="secondary" sx={{ height: '55px' }} onClick={() => handleRemoveParams()}>Clear Filter</Button> : ''}
            </div>
            {params ? <h2>{genresArr
                .filter((genre) => params.genres.includes(genre.id))
                .map((genre) => genre.name)
                .join(", ")}</h2>
                : ""}

        </>
    );
}

export default Search;

