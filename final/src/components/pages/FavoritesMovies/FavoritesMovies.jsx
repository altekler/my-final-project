import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Container } from '@mui/material';
import { fetchMoviesById } from '../../../thunk/movies';
import MoviesList from '../../MoviesList/MoviesList';


function FavoritesMovies() {
  const favoritesMovies = useSelector((state) => state.userReducer.user.favoritesMovies);

  const dispatch = useDispatch();



  useEffect(() => {
    dispatch(fetchMoviesById(favoritesMovies))
  }, [favoritesMovies, dispatch]);

  return (
    <>
      <h1>Favorites Movies</h1>
      <Container>
        <MoviesList />
      </Container>
    </>)
}

export default FavoritesMovies;