import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { fetchMovieById } from '../../../thunk/movies';
import { useNavigate } from 'react-router-dom';
import './MoviePage.scss'
import CircularProgress from '@mui/material/CircularProgress';
import Fab from '@mui/material/Fab';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import DateRangeIcon from '@mui/icons-material/DateRange';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Backdrop from '@mui/material/Backdrop';
import { addFavorites, removeFavorites } from '../../../actions/userActions';
const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 800,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  height: "500px"

};

function MoviePage() {
  const favoritesMovies = useSelector((state) => state.userReducer.user.favoritesMovies);
  const [buttonName, setButtonName] = useState(false);
  const dispatch = useDispatch();
  const { id } = useParams();
  const movieId = parseInt(id);
  const navigate = useNavigate();
  let languagesArr = useSelector(state => state.moviesReducer.languages);

  let checkFavorit = favoritesMovies.some(itemid => itemid === movieId);
  useEffect(() => {
    dispatch(fetchMovieById(movieId));
  }, [movieId, dispatch]);
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const movie = useSelector((state) => state.moviesReducer.movie);
  const videoKey = useSelector((state) => state.moviesReducer.video);


  function goBack() {
    navigate(-1);
  }

  useEffect(() => {
    if (checkFavorit) {
      setButtonName('Remove Favorite')
    } else {
      setButtonName('Add Favorite')
    }
  }, [checkFavorit]);

  const favoritesOptional = (movieId) => {
    if (!checkFavorit) {
      dispatch(addFavorites(movieId));
    } else {
      dispatch(removeFavorites(movieId));
    }
    console.log(favoritesMovies);
  }

  if (!movie) {
    return <CircularProgress />;
  }
  let languageMovie = languagesArr.find(
    (language) => language.iso_639_1 === movie.original_language
  ).english_name;


  const genreNames = movie.genres.map(genre => genre.name).join(', ');
  return (
    <>
      <Fab color="primary" size='small' aria-label="add" onClick={() => goBack()} className='button-back'  style={{position: 'absolute'}}>
        <ArrowBackIcon />
      </Fab>
      <section className='header__movie-page' style={{
        backgroundImage: `url(https://image.tmdb.org/t/p/w500${movie['backdrop_path']})`,
      }}>

      </section>
      <div className='main-section' id="main-section">
        <img
          alt={movie.title}
          height="400"
          src={'https://image.tmdb.org/t/p/w500' + movie['poster_path']}
          className='movie-image'
        />
        <div className='movie-info'>
          <h1 className='movie-title'>{movie.title}</h1>
          <h3 className='movie-title__small'>Release Date</h3>
          <p className='movie-text'><DateRangeIcon />{movie.release_date}</p>
          <h3 className='movie-title__small'>Original Language</h3>
          <p className='movie-text'>{languageMovie}</p>
          <h3 className='movie-title__small'>Runtime</h3>
          <p className='movie-text'>{movie.runtime} min</p>
          <h3 className='movie-title__small'>Gengre</h3>
          <p className='movie-text'>{genreNames}</p>
          <h3 className='movie-title__small'>Overview</h3>
          <p className='movie-text'>{movie.overview}</p>
          <ButtonGroup variant="text" aria-label="text button group" sx={{ mt: '30px' }}>
            {videoKey ? <Button onClick={handleOpen}>Trailer</Button> : ''}
            <Button onClick={() => favoritesOptional(movieId)}>{buttonName}</Button>
          </ButtonGroup>
        </div>
      </div>

      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Box sx={style}>
          <iframe
            width="800"
            height="500"
            src={"https://www.youtube.com/embed/" + videoKey}
            title="YouTube video player"
            frameBorder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            allowFullScreen
          ></iframe>
        </Box>
      </Modal>


    </>
  );
}

export default MoviePage;
