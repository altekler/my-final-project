import React, { useEffect } from "react";
import FormInput from "../../componentsForm/FormInput";
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import InvalidText from "../../componentsForm/InvalidText";
import { Link, useNavigate } from 'react-router-dom';
import RadioButton from "../../componentsForm/RadioButton";
import RadioGroup from '@mui/material/RadioGroup';
import FormLabel from '@mui/material/FormLabel';
import '../../componentsForm/style.scss'
import { useDispatch, useSelector } from 'react-redux';
import { singUp } from "../../../actions/userActions";
const schema = yup
  .object({
    firstName: yup.string().required('Please enter your first name').matches(/^[A-Za-z]+$/, 'First name can only contain English letters'),
    lastName: yup.string().required('Please enter your last name').matches(/^[A-Za-z]+$/, 'Last name can only contain English letters'),
    email: yup.string().required('Please enter your email').email('Please enter a valid email address'),
    dateOfBirth: yup
      .string()
      .required('Please select a date of birth'),
    gender: yup
      .string()
      .required('Please select a gender option'),
    userName: yup
      .string()
      .required('Please enter your user name')
      .matches(
        /^[a-z][a-zA-Z0-9_.]*$/,
        'User name must start with a lowercase letter, contain only letters, numbers, and special characters (dot or underscore)'
      ),
    password: yup
      .string()
      .required('Please enter your password')
      .matches(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
        'Password must be at least 8 characters, contain at least one number, one lowercase and one uppercase letter'
      ),
    passwordConfirm: yup
      .string()
      .required('Please confirm your password')
      .oneOf([yup.ref('password')], 'Passwords must match'),
  })
  .required();

function SingUpPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const user = useSelector((state) => state.userReducer.user);
  const err = useSelector((state) => state.userReducer.err);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  useEffect(() => {
    if (user) {
      navigate('/home');
    }
  }, [user, navigate]);
  
  const onSubmit = (data) => {
    const { passwordConfirm, ...dataToSend } = data;
    let newUser = { ...dataToSend, favoritesMovies: [] }
    dispatch(singUp(newUser))
  };
  return (
    <form noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)} className="form-reg">
      <Typography variant="h3" gutterBottom>
        Sing Up</Typography>
      <FormInput
        label="First Name"
        {...register('firstName')}
        type="text"
      />
      <InvalidText>{errors.firstName?.message}</InvalidText>
      <FormInput
        label="Last Name"
        {...register('lastName')}
        type="text"
      />
      <InvalidText>{errors.lastName?.message}</InvalidText>
      <FormInput
        label="User Name"
        {...register('userName')}
        type="text"
      />
      <InvalidText>{errors.userName?.message}</InvalidText>
      {err ? <InvalidText>{err}</InvalidText> : ''}
      <FormLabel sx={{ color: "#1976d2" }}>Date of Birth</FormLabel>
      <FormInput
        {...register('dateOfBirth')}
        type="date"
        placeholder=''
      />
      <InvalidText >{errors.dateOfBirth?.message}</InvalidText>
      <FormLabel sx={{ color: "#1976d2" }}>Gender</FormLabel>
      <RadioGroup
        row
        aria-labelledby="demo-row-radio-buttons-group-label"
        name="row-radio-buttons-group"
        className="gender"
      >
        <RadioButton value="male" label="Male"  {...register('gender')} />
        <RadioButton value="female" label="Female"  {...register('gender')} />
      </RadioGroup>
      <InvalidText >{errors.gender?.message}</InvalidText>
      <FormInput
        label="Email"
        {...register('email')}
        type="email"
      />
      <InvalidText>{errors.email?.message}</InvalidText>
      <FormInput
        label="Password"
        {...register('password')}
        type="password"
      />
      <InvalidText>{errors.password?.message}</InvalidText>
      <FormInput
        label="Password Confirm"
        {...register('passwordConfirm')}
        type="password"
      />
      <InvalidText>{errors.passwordConfirm?.message}</InvalidText>
      <div className="form__footer">  <Button type="submit">
        Sing Up
      </Button>
        <Link to="/singin" className="small__text-form"> Log in?</Link></div>

    </form>
  );
}


export default SingUpPage;