import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Box, Container } from '@mui/material';
import Pagination from '@mui/material/Pagination';
import { fetchMovies, fetchSearchMovies } from '../../../thunk/movies';
import MoviesList from '../../MoviesList/MoviesList';
import './MoviesPage.scss'
import Search from '../../Search/Search';
import { setCurrentPage } from '../../../actions/moviesActions';
function MoviesPage() {
  const totalPages = useSelector((state) => state.moviesReducer.totalPages);
  const currentPage = useSelector((state) => state.moviesReducer.currentPage);
  const dispatch = useDispatch();

  const params = useSelector((state) => state.moviesReducer.params);
  useEffect(() => {
    if (params) {
      dispatch(fetchSearchMovies({ ...params, page: currentPage }))
    } else {
      dispatch(fetchMovies({ page: currentPage }))
    }

  }, [currentPage, dispatch, params]);


  const handlePaginationChange = (_, value) => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    dispatch(setCurrentPage(value));
  };

  return (
    <>
      <h1>Movies</h1>
      <Search />
      <Container>
        <MoviesList />
        <Box display="flex" justifyContent="center" my={4}>
          <Pagination
            count={Math.min(totalPages, 500)}
            page={currentPage}
            onChange={handlePaginationChange}
            color="primary"
          />
        </Box>
      </Container>
    </>
  );
}

export default MoviesPage;
