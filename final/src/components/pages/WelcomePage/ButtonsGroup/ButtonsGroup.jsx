import React from 'react';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';

function ButtonsGroup() {
  return (
    <div className='wrapper-button'>
      <Link to="/singin">
        <Button variant="contained" sx={{ background: "#42a5f5", mr: "10px" }}>
          Sing in
        </Button>
      </Link>
      <Link to="/singup">
        <Button variant="contained" sx={{ background: "#90caf9" }}>
          Sing up
        </Button>
      </Link>
    </div>
  );
}

export default ButtonsGroup;