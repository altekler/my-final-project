import React from 'react';
import "./WelcomePage.scss"
import ButtonsGroup from './ButtonsGroup/ButtonsGroup';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import Button from '@mui/material/Button';

function WelcomePage() {
  const user = useSelector((state) => state.userReducer.user);
  let navigate = useNavigate()
  return (
    <>
      <h1 className='main-title'>
        Hello and welcome to my final project
      </h1>
      {user ? <Button size="large" onClick={() => navigate('/movies')}><span>Lets Go</span></Button> : <ButtonsGroup />}
    </>
  );
}

export default WelcomePage;