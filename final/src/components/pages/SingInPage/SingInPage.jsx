import React, { useEffect } from "react";
import FormInput from "../../componentsForm/FormInput";
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import InvalidText from "../../componentsForm/InvalidText";
import { Link, useNavigate } from 'react-router-dom';
import '../../componentsForm/style.scss'
import { useDispatch, useSelector } from 'react-redux';
import { singIn } from "../../../actions/userActions";

const schema = yup
  .object({
    userName: yup.string().required('Please enter your user name'),
    password: yup.string().required('Please enter your password'),
  })
  .required();

function SingInPage() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const user = useSelector((state) => state.userReducer.user); 
  const err = useSelector((state) => state.userReducer.err);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });
  useEffect(() => {
    if (user) {
      navigate('/home');
    }
  }, [user, navigate]);


  const onSubmit = (data) => {
    dispatch(singIn(data.userName, data.password));
  };
  return (
    <form noValidate autoComplete="off" onSubmit={handleSubmit(onSubmit)} >
      <Typography variant="h3" gutterBottom>
        Sing In</Typography>
        {err ? <InvalidText>{err}</InvalidText> : ''}
      <FormInput
        label="UserName"
        {...register('userName')}
        type="text"
      />
      <InvalidText>{errors.userName?.message}</InvalidText>
      <FormInput
        label="Password"
        {...register('password')}
        type="password"
      />
      <InvalidText>{errors.password?.message}</InvalidText>
      <div className="form__footer"><Button type="submit">
        Sing in
      </Button>
        <Link to="/singup" className="small__text-form"> Create an account</Link></div>
    </form>
  );
}

export default SingInPage;