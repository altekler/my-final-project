import React from 'react';
import HeaderMain from './HeaderMain/HeaderMain';
import { Outlet } from 'react-router-dom';
import SectionsWrapper from './SectionsWrapper/SectionsWrapper';

function WrapperMain() {
  return (
    <>
      <HeaderMain />
      <SectionsWrapper>
        <Outlet />
      </SectionsWrapper>
    </>
  )
}

export default WrapperMain;
