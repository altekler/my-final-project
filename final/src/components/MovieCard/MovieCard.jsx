import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Fab from '@mui/material/Fab';
import FavoriteIcon from '@mui/icons-material/Favorite';
import './MovieCard.scss';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from "@mui/material/Alert";
import { useSelector, useDispatch } from 'react-redux';
import { addFavorites, removeFavorites } from '../../actions/userActions';
import Rating from '@mui/material/Rating';
import DateRangeIcon from '@mui/icons-material/DateRange';
import { useNavigate } from 'react-router-dom';

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} variant="filled" ref={ref} {...props} />;
});


export default function MovieCard({ movie }) {
  const navigate = useNavigate()
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);
  const [alertMessage, setAlertMessage] = React.useState(false);
  const favoritesMovies = useSelector((state) => state.userReducer.user.favoritesMovies);
  let checkFavorit = favoritesMovies.some(id => id === movie.id)
  const [buttonColor, setButtonColor] = React.useState('outline');
  const ratingMovie = movie['vote_average'] / 2;

  React.useEffect(() => {
    if (checkFavorit) {
      setButtonColor('secondary');
      setAlertMessage('Movie added to favorites!')
    } else {
      setButtonColor('outline');
      setAlertMessage('Movie removed to favorites')
    }
  }, [checkFavorit]);

  const favoritesOptional = (movieId) => {
    if (checkFavorit) {
      dispatch(removeFavorites(movieId))
    } else {
      dispatch(addFavorites(movieId))
    }
  }
  const handleOpen = () => {
    setOpen(true);
    favoritesOptional(movie.id)
  };
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  const selectedMovie = () => {
    navigate(`/movie/${movie.id}`)
  }
  return (
    <>
      <Card sx={{
        maxWidth: 345,
        background: '#283593',
        transition: 'transform 0.3s , box-shadow 0.3s',
        '&:hover': {
          transform: 'scale(1.05)',
          boxShadow: '0px 0px 30px -2px rgba(25,118,210,1)'
        },
        position: 'relative'
      }}>
        <CardMedia
          component="img"
          alt={movie.title}
          height="400"
          onClick={() => selectedMovie()}
          image={'https://image.tmdb.org/t/p/w500' + movie['poster_path']}
          style={{cursor: "pointer"}}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div" color="white" sx={{ mt: '15px', }}>
            {movie.title}
          </Typography>
          <Typography variant="body2" color="white" sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
            <DateRangeIcon />{movie['release_date']}
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" sx={{ color: 'white' }} onClick={() => selectedMovie()}>Learn More</Button>
          <Rating name="half-rating-read" value={ratingMovie} precision={0.1} size="medium" sx={{ ml: 'auto' }} readOnly />
        </CardActions>
        <div className='card__optional'>
          <Fab color={buttonColor} size="medium" aria-label="like" onClick={() => handleOpen()}>
            <FavoriteIcon />
          </Fab>
        </div>
      </Card>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} color="secondary" severity="success" sx={{ width: '100%' }}>
          {alertMessage}
        </Alert>
      </Snackbar>

    </>
  );
}