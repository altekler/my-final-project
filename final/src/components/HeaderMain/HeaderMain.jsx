import React from 'react';
import Container from '@mui/material/Container';
import TheatersIcon from '@mui/icons-material/Theaters';
import "./HeaderMain.scss"
import { useSelector } from 'react-redux';
import HeaderOptional from './HeaderOptional/HeaderOptional';
import { Link } from 'react-router-dom';

function HeaderMain() {
  const user = useSelector((state) => state.userReducer.user);
  return (
    <Container maxWidth="sl" sx={{ p: "10px" }}>
      <header className='header-main'>
        <Link to="/home" className='wrapper-name-site'>
          <TheatersIcon sx={{ color: "#fefefe", width: "50px", height: "50px" }} />
          <h2 className='title-name-site'>Final Project</h2>
        </Link>
        {user ? <HeaderOptional /> : ''}
      </header>
    </Container>

  );
}

export default HeaderMain;