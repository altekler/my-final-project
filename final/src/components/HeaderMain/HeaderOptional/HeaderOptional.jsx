import React from 'react';
import "./HeaderOptional.scss"
import Button from '@mui/material/Button';
import { useNavigate } from 'react-router-dom';
import MovieIcon from '@mui/icons-material/Movie';
import FavoriteIcon from '@mui/icons-material/Favorite';
import { useSelector, useDispatch } from 'react-redux';
import { logOut } from '../../../actions/userActions';
import { setCurrentPage, removeParams } from '../../../actions/moviesActions';
function HeaderOptional() {
  const dispatch = useDispatch();
  let navigate = useNavigate()
  const userName = useSelector((state) => state.userReducer.user.userName);
  function handleLogOut() {
    dispatch(logOut())
  }
  function openMovies() {
    navigate('/movies');
    dispatch(setCurrentPage('1'));
    dispatch(removeParams());
  }
  return (
    <div className='optional'>
      <nav className='nav'>
        <Button className='nav__button' size="large" onClick={() => openMovies()}><MovieIcon /> <span>Movies</span></Button>
        <Button className='nav__button' size="large" onClick={() => navigate('/favorites')}><FavoriteIcon /><span>Favorites</span> </Button>
      </nav>
      <p className='optional__text'>Hello, {userName}</p>
      <Button variant="contained" sx={{ background: "#1a237e" }} onClick={() => handleLogOut()}>
        Log out
      </Button>
    </div>
  );
}

export default HeaderOptional;