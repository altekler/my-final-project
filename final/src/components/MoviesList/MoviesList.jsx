import React from 'react';
import Grid from '@mui/material/Grid';
import { useSelector } from 'react-redux';
import CircularProgress from '@mui/material/CircularProgress';
import MovieCard from '../MovieCard/MovieCard';

function MoviesList() {
    const movies = useSelector((state) => state.moviesReducer.movies);

    return (
        <>
            <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }}>
                {movies ? (
                    movies.map((movie) => (
                        <Grid item xs={2} sm={4} md={4} key={movie.id}>
                            <MovieCard movie={movie} />
                        </Grid>))
                ) : (
                    <CircularProgress />
                )}
            </Grid>
        </>
    );
}

export default MoviesList;
