import React from 'react';
import Container from '@mui/material/Container';
import "./SectionsWrapper.scss"
function SectionsWrapper({ children }) {
  return (

    <Container maxWidth="sl" sx={{ p: "10px" }}>
      <div className='sections-wrapper'>
        {children}
      </div>
    </Container>

  );
}

export default SectionsWrapper;