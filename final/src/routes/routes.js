import  { Navigate, createBrowserRouter}  from 'react-router-dom';
import WrapperMain from '../components/WrapperMain';
import WelcomePage from '../components/pages/WelcomePage/WelcomePage';
import SingInPage from '../components/pages/SingInPage/SingInPage';
import SingUpPage from '../components/pages/SingUpPage/SingUpPage';
import FavoritesMovies from '../components/pages/FavoritesMovies/FavoritesMovies';
import MoviePage from '../components/pages/MoviePage/MoviePage';
import MoviesPage from '../components/pages/MoviesPage/MoviesPage';
import ProtectedRoute from './ProtectedRoute';
export const router = createBrowserRouter([
  {
    path: '/',
    element: (
      <WrapperMain />
    ),
    children: [
        {
          path: '/',
          element: <Navigate to="/home" replace />
        },
        {
          path: '/home',
          element: <WelcomePage />
        },
        {
          path: '/singin',
          element: <SingInPage />
        }, 
        {
          path: '/singup',
          element: <SingUpPage />
        },
        {
          path: '/favorites',
          element:<ProtectedRoute><FavoritesMovies /></ProtectedRoute> 
        },
        {
          path: '/movie/:id',
          element: <ProtectedRoute><MoviePage /></ProtectedRoute>
        },
        {
          path: '/movies',
          element:<ProtectedRoute><MoviesPage /></ProtectedRoute>
        
        },
    ]
  },
]);

