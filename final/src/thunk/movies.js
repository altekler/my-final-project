import { getMoviesList, getMovieById, getTrailerById, getGenresList, getLanguagesList, SearchMovies } from "../api/tmdb";
import { loadMovies, loadFavoriteMovie, loadMovie, loadVideo, loadGenres, loadLanguages } from "../actions/moviesActions";

export const fetchMovies = (params) => {
    return async (dispatch) => {
        try {
            const { data: movies } = await getMoviesList(params);
            dispatch(loadMovies(movies.results, movies['total_pages']))
        } catch (e) {
            console.log(e)
        }
    }
}
export const fetchMoviesById = (arrId) => {
    return async (dispatch) => {
        try {
            let favoritesMovies = []
            for (let id of arrId) {
                const { data: movie } = await getMovieById(id);
                favoritesMovies.push(movie)
            }
            dispatch(loadFavoriteMovie(favoritesMovies))
        } catch (e) {
            console.log(e)
        }
    }
}

export const fetchMovieById = (id) => {
    return async (dispatch) => {
        try {
            const { data: movie } = await getMovieById(id);
            const { data: video } = await getTrailerById(id);
            if (video.results && video.results[0]) {
                dispatch(loadVideo(video.results[0].key));
            } else {
                dispatch(loadVideo(null));
            }
            dispatch(loadMovie(movie));
        } catch (e) {
            console.log(e);
        }
    };
};
export const fetchGenres = () => {
    return async (dispatch) => {
        try {
            const { data } = await getGenresList();
            dispatch(loadGenres(data.genres))
        } catch (e) {
            console.log(e)
        }
    }
}
export const fetchLanguage = () => {
    return async (dispatch) => {
        try {
            const { data } = await getLanguagesList();
            dispatch(loadLanguages(data))
        } catch (e) {
            console.log(e)
        }
    }
}
export const fetchSearchMovies = (params) => {
    return async (dispatch) => {
        try {
            const { data: movies } = await SearchMovies(params);
            dispatch(loadMovies(movies.results, movies['total_pages']))
        } catch (e) {
            console.log(e)
        }
    }
}

