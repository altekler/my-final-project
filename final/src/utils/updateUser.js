export function updateUser(user, users){
    if (user) {
        localStorage.setItem('user', JSON.stringify(user));
      } else if(users) {
        localStorage.setItem('users', JSON.stringify(users));   
        localStorage.removeItem('user');
      }
}