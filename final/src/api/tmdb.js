import axios from 'axios';
import { ACCESS_KEY } from '../config/config';

const tmdbAPI = axios.create({
  baseURL: 'https://api.themoviedb.org/3',
});

export function getMoviesList(params = {}) {
  return tmdbAPI.get('/discover/movie', {
    params: {
      api_key: ACCESS_KEY,
      ...params,
    },
  });
}
export function getMovieById(id) {
  return tmdbAPI.get(`/movie/${id}`, {
    params: {
      api_key: ACCESS_KEY,
    },
  });
}
export function getTrailerById(id) {
  return tmdbAPI.get(`/movie/${id}/videos`, {
    params: {
      api_key: ACCESS_KEY,
      language: 'en-US',
    },
  });
}
export function getGenresList() {
  return tmdbAPI.get(`/genre/movie/list`, {
    params: {
      api_key: ACCESS_KEY
    },
  });
}
export function getLanguagesList() {
  return tmdbAPI.get(`/configuration/languages`, {
    params: {
      api_key: ACCESS_KEY
    },
  });
}
export function SearchMovies(params) {
  if (params.query) {
    return tmdbAPI.get('/search/movie', {
      params: {
        api_key: ACCESS_KEY,
        page: params.page,
        query: params.query,
      }
    })
  } else {
    return tmdbAPI.get('/discover/movie', {
      params: {
        api_key: ACCESS_KEY,
        with_original_language: params.language || 'en',
        with_genres: params.genres.join(','),
        page: params.page
      },
    });
  }
}