export const loadMovies = (movies, totalPages) => ({
  type: 'LOAD_MOVIES',
  payload: { movies, totalPages },
});
export const loadFavoriteMovie = (movies) => ({
  type: 'LOAD_FAVORITE_MOVIE',
  payload: { movies },
});
export const loadMovie = (movie) => ({
  type: 'LOAD_MOVIE',
  payload: { movie },
})
export const loadVideo = (video) => ({
  type: 'LOAD_VIDEO',
  payload: { video },
})
export const loadGenres = (genres) => ({
  type: 'LOAD_GENRES',
  payload: { genres },
})
export const loadLanguages = (languages) => ({
  type: 'LOAD_LANGUAGES',
  payload: { languages },
})
export const loadParams = (params) => ({
  type: 'LOAD_PARAMS',
  payload: { params },
})
export const removeParams = () => ({
  type: 'REMOVE_PARAMS',
})
export const setCurrentPage = (page) => ({
  type: 'SET_CURRENT_PAGE',
  payload: { page },
})