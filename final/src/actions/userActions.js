export const logOut = () => ({
  type: 'LOG_OUT'
});
export const singIn = (userName, password) => ({
  type: 'SING_IN',
  payload: { userName, password },
});
export const singUp = (newUser) => ({
  type: 'SING_UP',
  payload: { newUser },
});
export const addFavorites = (movieId) => ({
  type: 'ADD_FAVORIT',
  payload: { movieId },
});
export const removeFavorites = (movieId) => ({
  type: 'REMOVE_FAVORIT',
  payload: { movieId },
});